# Keycloak Account API PHP bindings

This repository is the very initial outline for interfacing with the Keycloak account_api.

## Usage

To add this to your project: 

`composer require aria-php/keycloak-api`

## Supported features

This API wrapper supports the ability to:

### Account API

* Retrieve the profile of your user (as identified by your `access_token` sent as a bearer token)
* Update the profile
* Set the user's password

e.g.

```
use ARIA\KeycloakAPI\AccountAPI;

...

$api = new AccountAPI($client_id, $secret, 'master', 'http://localhost:8080');
$api->setBearer($access_token);


$profile = $api->getProfile();


// Make your updates here


$api->setProfile($profile);


```

## Authentication

Your user is identified by obtaining an access token from keycloak for the user, and then passing it as a bearer token. 

Note: this is the `access_token`, not the `id_token`.

## Testing 

In order to run the unit tests you need to set up some environment variables:

* `KEYCLOAK_USER` is the user you're trying to connect to 
* `KEYCLOAK_PASS` their password
* `KEYCLOAK_REALM` the keycloak realm (e.g. "master")
* `KEYCLOAK_CLIENT_ID` the keycloak client id (e.g. "account")
* `KEYCLOAK_SECRET` the client secret
* `KEYCLOAK_BASEURL` the base URL, e.g. "http://localhost:8080"

You will also need to enable "Direct access grants" for your client.

## Licence 

LGPLv3 - Please see LICENSE.txt for details.