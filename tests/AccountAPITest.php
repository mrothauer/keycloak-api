<?php

use ARIA\KeycloakAPI\AccountAPI;

class AccountAPITest extends \PHPUnit\Framework\TestCase {
  
  use Bearer;
  
  private $api;
  private $token;


  public function setUp(): void {
    
    $username = getenv('KEYCLOAK_USER');
    $password = getenv('KEYCLOAK_PASS');
    $realm = getenv('KEYCLOAK_REALM')?? 'master';
    $client_id = getenv('KEYCLOAK_CLIENT_ID');
    $secret = getenv('KEYCLOAK_SECRET');
    $baseurl = getenv('KEYCLOAK_BASEURL');
    
    if (empty($username)) throw new \RuntimeException('Please define the KEYCLOAK_USER environment variable.');
    if (empty($password)) throw new \RuntimeException('Please define the KEYCLOAK_PASS environment variable.');
    if (empty($realm)) throw new \RuntimeException('Please define the KEYCLOAK_REALM environment variable.');
    if (empty($client_id)) throw new \RuntimeException('Please define the KEYCLOAK_CLIENT_ID environment variable.');
    if (empty($secret)) throw new \RuntimeException('Please define the KEYCLOAK_SECRET environment variable.');
    if (empty($baseurl)) throw new \RuntimeException('Please define the KEYCLOAK_BASEURL environment variable.');
    
    $this->api = new AccountAPI($client_id, $secret, $realm, $baseurl);
    $this->token = $this->getAccessToken($this->api, $username, $password);
    $this->api->setBearer($this->token);
  }

  public function tearDown(): void {

    
  }

  
  public function testGetProfile() {
    
    $profile = $this->api->getProfile();
    
    $this->assertNotEmpty($profile);
    $this->assertEquals(getenv('KEYCLOAK_USER'), $profile['username']);
    
    return $profile;
  }
  
  
  /*public function testSetProfile() {
    
    $profile = $this->api->getProfile();
    
    $this->assertNotEmpty($profile);
    $this->assertEquals(getenv('KEYCLOAK_USER'), $profile['username']);
    
    $orig = $profile['firstName'];
    
    $profile['firstName'] = 'updated';
    
    $this->api->setProfile($profile);
    
    $profile = $this->api->getProfile();
    
    $this->assertNotEmpty($profile);
    $this->assertEquals($profile['firstName'], 'updated');
    
    
    // Restore
    $profile['firstName'] = $orig;
    $this->api->setProfile($profile);
    
    $profile = $this->api->getProfile();
    
    
    $this->assertNotEmpty($profile);
    $this->assertEquals($profile['firstName'], $orig);
  } */
  
  
 /* public function testSetPassword() {
    
    $this->api->setPassword(getenv('KEYCLOAK_PASS'), 'newtesttest', 'newtesttest');
    
    $this->api->setPassword('newtesttest', getenv('KEYCLOAK_PASS'), getenv('KEYCLOAK_PASS'));
    
    
    
  }*/

  public function testGetApplications() {

    $applications = $this->api->getApplications();

    $this->assertNotEmpty($applications);

  }

  public function testGetApplicationConsents() {

    $consents = $this->api->getApplicationConsents();

    $this->assertNotEmpty($consents);

  }
  
}