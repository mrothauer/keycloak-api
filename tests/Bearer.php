<?php


use GuzzleHttp\Client;


trait Bearer {
  
  static $bearer = [];
  

  public function getAccessToken(ARIA\KeycloakAPI\Keycloak $api, string $username = null, string $password = null, string $scope = 'account') : ? string {
    
    if (empty($username) || empty($password)) {
      throw new \RuntimeException("Keycloak Username or Password not found.");
    }
    
    if (!empty(self::$bearer[$username])) {
      return self::$bearer[$username];
    }
    
    $client = new Client();
    
    $response = $client->request('POST', $api->getBaseUrl() . 'auth/realms/' . $api->getRealm() . '/protocol/openid-connect/token', [

        'form_params' => [
            'grant_type' => 'password',
            'client_id' => $api->getClientID(),
            'password' => $password,
            'username' => $username,
            'client_secret' => $api->getSecret(),
            'scope' => $scope
        ]

    ]);
    
    $result = json_decode($response->getBody()->getContents(), true);
    
    self::$bearer[$username] = $result['access_token'];
    
    return $result['access_token'];
    
  }
  
}
