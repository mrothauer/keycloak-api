<?php

namespace ARIA\KeycloakAPI;

use GuzzleHttp\Client;

/**
 * Keycloak API base class.
 */
abstract class Keycloak {
    
  /**
   * Keycloak realm
   * @var string
   */
  private $realm;
  
  /**
   * Client ID
   * @var string
   */
  private $client_id;
  
  /**
   * Keycloak client secret key
   * @var string
   */
  private $secret; 
  
  /**
   * Keycloak base url
   * @var string
   */
  private $baseurl;
  
  /**
   * Temporary variable holding the access_token
   * @var type 
   */
  private $bearer;
  
  /**
   * Configure the realm being used
   * @param string $realm
   */
  public function setRealm(string $realm) {
    $this->realm = $realm;
  }
  
  /**
   * Configure the client id
   * @param string $client_id
   */
  public function setClientID(string $client_id) {
    $this->client_id = $client_id;
  }
  
  /**
   * Configure the secret key
   * @param string $secret
   */
  public function setSecret(string $secret) {
    $this->secret = $secret;
  }
  
  /**
   * Configure the base url
   * @param string $baseurl
   */
  public function setBaseUrl(string $baseurl) {
    $this->baseurl = rtrim($baseurl, '/') . '/';
  }
  
  /**
   * Configure the bearer token
   * @param string $bearer
   */
  public function setBearer(string $bearer) {
    $this->bearer = $bearer;
  }
  
  /**
   * Retrieve the realm
   * @return string
   */
  public function getRealm() : string {
    return $this->realm;
  }
  
  /**
   * Retrieve the client ID
   * @return string
   */
  public function getClientID() : string {
    return $this->client_id;
  }
  
  /**
   * Retrieve the secret key
   * @return string
   */
  public function getSecret() : string {
    return $this->secret;
  }
  
  /**
   * Retrieve the base URL being used
   * @return string
   */
  public function getBaseUrl() : string {
    return $this->baseurl;
  }
  
  /**
   * Retrieve the bearer token
   * @return string|null
   */
  public function getBearer() : ? string {
    return $this->bearer;
  }
  
  /**
   * Perform a low level GET query
   * @param string $endpoint The endpoint being called, or '' for the base URL.
   * @return array|null Decoded return
   * @throws \RuntimeException
   */
  protected function doGET(string $endpoint = '') : ?array {
    
    $endpoint = trim($endpoint, ' /');
    
    $bearer = $this->getBearer();
    if (empty($bearer)) {
      throw new \RuntimeException('Bearer token is unavailable.');
    }
    
    $client = new Client();
    
    $response = $client->request('GET', $this->getAPIUrl() . $endpoint, [
        'headers' => [
            'Authorization' => 'Bearer ' . $bearer,  
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ]
    ]);
    
    $body = json_decode($response->getBody()->getContents(), true);
    
    return $body;
    
  }
  
  /**
   * Perform a low level POST request to the endpoint.
   * @param string $endpoint The endpoint being called, or '' for the base URL
   * @param array $body An array representing the data being sent to the endpoint
   * @return array|null Return data
   * @throws \RuntimeException
   */
  protected function doPOST(string $endpoint = '', array $body = []) : ?array {
    
    $endpoint = trim($endpoint, ' /');
    
    $bearer = $this->getBearer();
    if (empty($bearer)) {
      throw new \RuntimeException('Bearer token is unavailable.');
    }
    
    $client = new Client();
    
    $response = $client->request('POST', $this->getAPIUrl() . $endpoint, [
        'headers' => [
            'Authorization' => 'Bearer ' . $bearer,  
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ],
        'json' => $body
    ]);
    
    $body = json_decode($response->getBody()->getContents(), true);
    
    return $body;
  } 
  
  /**
   * Endpoint returning the base URL of the particular API in question.
   */
  public abstract function getAPIUrl() : string;
    
}
