<?php

namespace ARIA\KeycloakAPI;

/**
 * Wrapper around Keycloak's experimental Account Management API
 */
class AccountAPI extends Keycloak {
  
  /**
   * Create a new instance of the Account API interface.
   * @param string $client_id
   * @param string $secret
   * @param string $realm
   * @param string $baseurl
   */
  public function __construct(string $client_id, string $secret, string $realm, string $baseurl) {
    
    $this->setClientID($client_id);
    $this->setSecret($secret);
    $this->setRealm($realm);
    $this->setBaseUrl($baseurl);
    
  }
  
  /**
   * Retrieve the base URL of the Accounts management API
   * @return string
   */
  public function getAPIUrl(): string {
    
    return $this->getBaseUrl() . 'auth/realms/' . $this->getRealm() . '/account/';
    
  }
  
  /**
   * Retrieve the user's profile
   * @return array|null
   */
  public function getProfile(): ? array {
    
    return $this->doGET();
    
  }
  
  /**
   * Update the user's profile
   * @param array $profile
   * @return bool
   */
  public function setProfile( array $profile, $removeUsername=true ) : bool {
    
    // if "editing username" is enabled in keycloak's realm settings, the username can be changed
    if ($removeUsername) {
      unset( $profile['username'] );
    }
    
    $result = $this->doPOST('', $profile); // POST will throw exception if result isn't 200
    
    return true;
    
  }

  /**
   * Set the password for the current user.
   * @param string $old
   * @param string $new
   * @param string $confirmation
   * @return bool
   * @throws \RuntimeException
   */
  public function setPassword( string $old, string $new, string $confirmation) : bool {
    
    if ($new != $confirmation) {
      throw new \RuntimeException('New password and confirmation password do not match');
    }
    
    $result = $this->doPOST('/credentials/password/', [
        
      "currentPassword" => $old,
      "newPassword" => $new,
      "confirmation" => $confirmation
            
    ]); // POST will throw exception if result isn't 200
    
    return true;
    
  }

  /**
   * Retrieve application details for the current user.
   *
   * @return array|null
   */
  public function getApplications() : ? array {

    return $this->doGet('/applications');

  }

  /**
   * Retrieve consents for the current user on the current client.
   *
   * @param string $client_id Optionally, specify a client ID to return the consents granted to that client
   * @return array|null
   */
  public function getApplicationConsents( string $client_id = null) : ? array {

    if (empty($client_id)) {
      $client_id = $this->getClientID();
    }

    return $this->doGET('/applications/'. $client_id .'/consent');

  }
  
}
